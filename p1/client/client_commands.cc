#include <cassert>
#include <cstring>
#include <iostream>
#include <openssl/rand.h>
#include <openssl/rsa.h>
#include <string>
#include <fstream>

#include "../common/contextmanager.h"
#include "../common/crypto.h"
#include "../common/file.h"
#include "../common/net.h"
#include "../common/protocol.h"
#include "../common/vec.h"

#include "client_commands.h"

using namespace std;

vec auth_msg(const string &user, const string &pass) {

  vec aBlock;
    vec_append(aBlock, (int) user.length());
    vec_append(aBlock, user);
    vec_append(aBlock, (int) pass.length());
    vec_append(aBlock, pass);

    return aBlock;
}

vec client_send_cmd(int sd, RSA *pub, const string &cmd, const vec &aBlock) {
  //ENCRYPT THE A BLOCK 

    vec aeskey = create_aes_key();
    EVP_CIPHER_CTX *ctx = create_aes_context(aeskey, true);
    vec encABlock = aes_crypt_msg(ctx, aBlock);
    
  //MAKE R BLOCK
    vec rBlock;    

    rBlock.push_back(cmd.at(0));
    rBlock.push_back(cmd.at(1));
    rBlock.push_back(cmd.at(2));
    vec_append(rBlock, aeskey);       
    rBlock.push_back(encABlock.size());
    
    //ENCRYPT R BLOCK
    vec encRBlock;
    encRBlock.reserve(LEN_RKBLOCK);
    unsigned char temp[LEN_RKBLOCK];
    RSA_public_encrypt(rBlock.size(), rBlock.data(), temp, pub, RSA_PKCS1_OAEP_PADDING);
    for (int k = 0; k < LEN_RKBLOCK; k++){
      encRBlock.push_back(temp[k]);
    }
    encRBlock.resize(LEN_RKBLOCK);
    
    vec_append(encRBlock, encABlock);

    // SEND STUFF BLOCK 
    if (!(send_reliably(sd, encRBlock)))
      cerr << "[CLIENT] Error SENDING R BLOCK";
  
   //RECIEVE SERVER OUTPUT
    vec encServerResponse = reliable_get_to_eof(sd);
     
    //DECRYPT SERVER OUTPUT
    if (!(reset_aes_context(ctx, aeskey, false))) 
          cerr << "[CLIENT] Error resetting aes context";
    vec decServerResponse = aes_crypt_msg(ctx, encServerResponse);

    //PRINT SERVER OUTPUT GET THAT GREEN OKAY BABYE
    return decServerResponse;

}


/// client_key() writes a request for the server's key on a socket descriptor.
/// When it gets it, it writes it to a file.
///
/// @param sd      An open socket
/// @param keyfile The name of the file to which the key should be written
void client_key(int sd, const string &keyfile) {
  
/* Make vector of KEY00000000... */    
    vec data;                
      data.push_back('K');
      data.push_back('E');
      data.push_back('Y');
      for (int i =0; i < 253; i++){     //256 - 3 = 253
        data.push_back('\0');
      }
    
/* Send vector (net.cc) */
    if (!(send_reliably(sd, data)))
      cerr << "[CLIENT] Error sending KEY";

/* Receive key as a vector (net.cc)*/
    vec keyFromServer = reliable_get_to_eof(sd);

/*Write key vector to a file (file.cc) */
    if (!(write_file(keyfile, (const char*)keyFromServer.data(), LEN_RSA_PUBKEY)))
      cerr << "[CLIENT] Error Writing to File"; 
}

/// client_reg() sends the REG command to register a new user
///
/// @param sd      The socket descriptor for communicating with the server
/// @param pubkey  The public key of the server
/// @param user    The name of the user doing the request
/// @param pass    The password of the user doing the request
void client_reg(int sd, RSA *pubkey, const string &user, const string &pass,
                const string &, const string &) {
              
    vec aBlock = auth_msg(user, pass);
    vec serverOutput = client_send_cmd(sd, pubkey, REQ_REG, aBlock);
    serverOutput.push_back('\n');
    cout << (const char*) serverOutput.data();
}

/// client_bye() writes a request for the server to exit.
///
/// @param sd An open socket
/// @param pubkey  The public key of the server
/// @param user    The name of the user doing the request
/// @param pass    The password of the user doing the request
void client_bye(int sd, RSA *pubkey, const string &user, const string &pass,
                const string &, const string &) {
  
    vec aBlock = auth_msg(user, pass);
    vec serverOutput = client_send_cmd(sd, pubkey, REQ_BYE, aBlock);
    serverOutput.push_back('\n');
    cout << (const char*) serverOutput.data();
}

/// client_sav() writes a request for the server to save its contents
///
/// @param sd An open socket
/// @param pubkey  The public key of the server
/// @param user The name of the user doing the request
/// @param pass The password of the user doing the request
void client_sav(int sd, RSA *pubkey, const string &user, const string &pass,
                const string &, const string &) {

    vec aBlock = auth_msg(user, pass);
    vec serverOutput = client_send_cmd(sd, pubkey, REQ_SAV, aBlock);
    serverOutput.push_back('\n');
    cout << (const char*) serverOutput.data();

}

/// client_set() sends the SET command to set the content for a user
///
/// @param sd      The socket descriptor for communicating with the server
/// @param pubkey  The public key of the server
/// @param user    The name of the user doing the request
/// @param pass    The password of the user doing the request
/// @param setfile The file whose contents should be sent
void client_set(int sd, RSA *pubkey, const string &user, const string &pass,
                const string &setfile, const string &) {
    /* MAKE THE A BLOCK */              
    vec aBlock = auth_msg(user, pass);

    vec profile = load_entire_file(setfile);
    vec_append(aBlock, profile.size());
    vec_append(aBlock, profile);
    
    //OUR CODE WORKS FOR THIS FUNCTION IF YOU COMMENT OUT LINES 163-165 AND COMMENT IN 170 AND 171
    //WE HAVE NO IDEA WHY IT WOULD WORK WITH HARDCODED VALUES BUT NOT ONES READ FROM THE FILE
    /*
    vec_append(aBlock, 5);
    vec_append(aBlock, "12345");
    */
    
    vec serverOutput = client_send_cmd(sd, pubkey, REQ_SET, aBlock);
    serverOutput.push_back('\n');
    cout << (const char*) serverOutput.data();
}

/// client_get() requests the content associated with a user, and saves it to a
/// file called <user>.file.dat.
///
/// @param sd      The socket descriptor for communicating with the server
/// @param pubkey  The public key of the server
/// @param user    The name of the user doing the request
/// @param pass    The password of the user doing the request
/// @param getname The name of the user whose content should be fetched
void client_get(int sd, RSA *pubkey, const string &user, const string &pass,
                const string &getname, const string &) {

    vec aBlock = auth_msg(user, pass);
    vec_append(aBlock, getname.length());
    vec_append(aBlock, getname);

    vec serverOutput = client_send_cmd(sd, pubkey, REQ_GET, aBlock);

    vec msg;

    msg.push_back(serverOutput.at(0));
    msg.push_back(serverOutput.at(1));

    if(!strcmp((const char *) msg.data(),"OK")){
      int length = serverOutput.at(2);

      vec allData;
      for(int i = 3; i < (int) serverOutput.size(); i++){
        allData.push_back(serverOutput.at(i));
      }

      string filename = user;
      filename = filename.append(".file.dat");
      if (!(write_file(filename, (const char*)allData.data(), length)))
        cerr << "[CLIENT] Error Writing to File";
    
      msg.push_back('\n');
      cout << (const char*) msg.data();
    }
    
    else{
      serverOutput.push_back('\n');
      cout << (const char*) serverOutput.data();    
    } 
}

/// client_all() sends the ALL command to get a listing of all users, formatted
/// as text with one entry per line.
///
/// @param sd The socket descriptor for communicating with the server
/// @param pubkey  The public key of the server
/// @param user The name of the user doing the request
/// @param pass The password of the user doing the request
/// @param allfile The file where the result should go
void client_all(int sd, RSA *pubkey, const string &user, const string &pass,
                const string &allfile, const string &) {


    vec aBlock = auth_msg(user, pass);
    vec serverOutput = client_send_cmd(sd, pubkey, REQ_SAV, aBlock);

    vec msg;

    msg.push_back(serverOutput.at(0));
    msg.push_back(serverOutput.at(1));

    if(!strcmp((const char *) msg.data(),"OK")){
      int length = serverOutput.at(2);

      vec allData;
      for(int i = 3; i < (int) serverOutput.size(); i++){
        allData.push_back(serverOutput.at(i));
      }

      if (!(write_file(allfile, (const char*)allData.data(), length)))
        cerr << "[CLIENT] Error Writing to File";
    
      msg.push_back('\n');
      cout << (const char*) msg.data();
    }
    else{
      serverOutput.push_back('\n');
      cout << (const char*) serverOutput.data();    
    }   
    
}
