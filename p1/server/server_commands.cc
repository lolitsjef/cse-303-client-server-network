#include <string>

#include "../common/crypto.h"
#include "../common/net.h"
#include "../common/protocol.h"
#include "../common/vec.h"

#include "server_commands.h"
#include "server_storage.h"

using namespace std;

/// Respond to an ALL command by generating a list of all the usernames in the
/// Auth table and returning them, one per line.
///
/// @param sd      The socket onto which the result should be written
/// @param storage The Storage object, which contains the auth table
/// @param ctx     The AES encryption context
/// @param req     The unencrypted contents of the request
///
/// @returns false, to indicate that the server shouldn't stop
bool server_cmd_all(int sd, Storage &storage, EVP_CIPHER_CTX *ctx,
                    const vec &req) {
  //get user vector
  int userLength = req.at(0);
  vec user;
  for (int i = 1; i < userLength; i++){
    user.push_back(req.at(i));
  }

  //get password vector
  int passLength = req.at(userLength);
  vec pass;
  for (int i = userLength; i < userLength + passLength; i++){
    user.push_back(req.at(i));
  }

  //if authenticate
  if (storage.auth((char*) user.data(), (char*)pass.data())){
    //call all and return what all returns to the client
    pair<bool,vec> returnStuff;
    returnStuff = storage.get_all_users((char*) user.data(), (char*)pass.data());

    if (returnStuff.first) {
      vec responseMsg = aes_crypt_msg(ctx, RES_OK);
      vec_append(responseMsg, returnStuff.second);
      send_reliably(sd, (char*) responseMsg.data());
    }
    else {
      vec responseMsg = aes_crypt_msg(ctx, returnStuff.second);
      send_reliably(sd, (char*) responseMsg.data());

    }
  }
  else {
    vec responseMsg = aes_crypt_msg(ctx, RES_ERR_LOGIN);
    send_reliably(sd, (char*) responseMsg.data());
    return false;
  }
  return false;
}

/// Respond to a SET command by putting the provided data into the Auth table
///
/// @param sd      The socket onto which the result should be written
/// @param storage The Storage object, which contains the auth table
/// @param ctx     The AES encryption context
/// @param req     The unencrypted contents of the request
///
/// @returns false, to indicate that the server shouldn't stop
bool server_cmd_set(int sd, Storage &storage, EVP_CIPHER_CTX *ctx,
                    const vec &req) {
  //get user vector
  int userLength = req.at(0);
  vec user;
  for (int i = 1; i < userLength; i++){
    user.push_back(req.at(i));
  }

  //get password vector
  int passLength = req.at(userLength);
  vec pass;
  for (int i = userLength; i < userLength + passLength; i++){
    user.push_back(req.at(i));
  }

  //get data vector
  int dataLength = req.at(userLength + passLength);
  vec data;
  for (int i = userLength; i < userLength + passLength + dataLength; i++){
    data.push_back(req.at(i));
  }

  //call storage set function and then return what it returns to the client
  string returnMsg;
  vec returnVec;
  returnVec = storage.set_user_data((char*) user.data(), (char*)pass.data(), data);
  returnVec = aes_crypt_msg(ctx, returnVec);
  returnMsg = (char*) returnVec.data();
  send_reliably(sd, returnMsg);

  return false;
}

/// Respond to a GET command by getting the data for a user
///
/// @param sd      The socket onto which the result should be written
/// @param storage The Storage object, which contains the auth table
/// @param ctx     The AES encryption context
/// @param req     The unencrypted contents of the request
///
/// @returns false, to indicate that the server shouldn't stop
bool server_cmd_get(int sd, Storage &storage, EVP_CIPHER_CTX *ctx,
                    const vec &req) {

                       //get user vector
  int userLength = req.at(0);
  vec user;
  for (int i = 1; i < userLength; i++){
    user.push_back(req.at(i));
  }

  //get password vector
  int passLength = req.at(userLength);
  vec pass;
  for (int i = userLength; i < userLength + passLength; i++){
    user.push_back(req.at(i));
  }

  //get data vector
  int dataLength = req.at(userLength + passLength);
  vec data;
  for (int i = userLength; i < userLength + passLength + dataLength; i++){
    data.push_back(req.at(i));
  }


  //if authenticate
  if (storage.auth((char*) user.data(), (char*)pass.data())){
    //call all and return what all returns to the client
    pair<bool,vec> returnStuff;
    returnStuff = storage.get_user_data((char*) user.data(), (char*) pass.data(), (char*) data.data());

    if (returnStuff.first) {
      vec responseMsg = aes_crypt_msg(ctx, RES_OK);
      vec_append(responseMsg, returnStuff.second);
      send_reliably(sd, (char*) responseMsg.data());
    }
    else {
      vec responseMsg = aes_crypt_msg(ctx, returnStuff.second);
      send_reliably(sd, (char*) responseMsg.data());

    }
  }
  else {
    vec responseMsg = aes_crypt_msg(ctx, RES_ERR_LOGIN);
    send_reliably(sd, (char*) responseMsg.data());
    return false;
  }
  return false;
}

/// Respond to a REG command by trying to add a new user
///
/// @param sd      The socket onto which the result should be written
/// @param storage The Storage object, which contains the auth table
/// @param ctx     The AES encryption context
/// @param req     The unencrypted contents of the request
///
/// @returns false, to indicate that the server shouldn't stop
bool server_cmd_reg(int sd, Storage &storage, EVP_CIPHER_CTX *ctx,
                    const vec &req) {
  //get username vector
  int userLength = req.at(0);
  vec user;
  for (int i = 1; i < userLength; i++){
    user.push_back(req.at(i));
  }

  //get password vector
  int passLength = req.at(userLength);
  vec pass;
  for (int i = userLength; i < userLength + passLength; i++){
    user.push_back(req.at(i));
  }

  //call add user and send what it returns to the client
  vec responseMsg;
  if (!storage.add_user((char*) user.data(), (char*)pass.data())) 
    responseMsg = aes_crypt_msg(ctx, RES_ERR_USER_EXISTS);
  else
    responseMsg = aes_crypt_msg(ctx, RES_OK);
    
    
  send_reliably(sd, (char*) responseMsg.data());  
  return false;
}

/// In response to a request for a key, do a reliable send of the contents of
/// the pubfile
///
/// @param sd The socket on which to write the pubfile
/// @param pubfile A vector consisting of pubfile contents
void server_cmd_key(int sd, const vec &pubfile) {
    send_reliably(sd, pubfile);
}

/// Respond to a BYE command by returning false, but only if the user
/// authenticates
///
/// @param sd      The socket onto which the result should be written
/// @param storage The Storage object, which contains the auth table
/// @param ctx     The AES encryption context
/// @param req     The unencrypted contents of the request
///
/// @returns true, to indicate that the server should stop, or false on an error
bool server_cmd_bye(int sd, Storage &storage, EVP_CIPHER_CTX *ctx,
                    const vec &req) {
  //get user vector
  int userLength = req.at(0);
  vec user;
  for (int i = 1; i < userLength; i++){
    user.push_back(req.at(i));
  }

  //get password vector
  int passLength = req.at(userLength);
  vec pass;
  for (int i = userLength; i < userLength + passLength; i++){
    user.push_back(req.at(i));
  }

  //authenticate user and close socket if authenticates, else return auth error
  if (storage.auth((char*) user.data(), (char*)pass.data())){
    vec responseMsg = aes_crypt_msg(ctx, RES_OK);
    send_reliably(sd, (char*) responseMsg.data());
    return true;
  }
  else {
    vec responseMsg = aes_crypt_msg(ctx, RES_ERR_LOGIN);
    send_reliably(sd, (char*) responseMsg.data());
    return false;
  }
  return false;

  
}

/// Respond to a SAV command by persisting the file, but only if the user
/// authenticates
///
/// @param sd      The socket onto which the result should be written
/// @param storage The Storage object, which contains the auth table
/// @param ctx     The AES encryption context
/// @param req     The unencrypted contents of the request
///
/// @returns false, to indicate that the server shouldn't stop
bool server_cmd_sav(int sd, Storage &storage, EVP_CIPHER_CTX *ctx,
                    const vec &req) {
  //get user vector
  int userLength = req.at(0);
  vec user;
  for (int i = 1; i < userLength; i++){
    user.push_back(req.at(i));
  }

  //get password vector
  int passLength = req.at(userLength);
  vec pass;
  for (int i = userLength; i < userLength + passLength; i++){
    user.push_back(req.at(i));
  }

  //authenticate the user and call persist, then send to client
  if (storage.auth((char*) user.data(), (char*)pass.data())){
    storage.persist();
    vec responseMsg = aes_crypt_msg(ctx, RES_OK);
    send_reliably(sd, (char*) responseMsg.data());
  }
  else {
    vec responseMsg = aes_crypt_msg(ctx, RES_ERR_LOGIN);
    send_reliably(sd, (char*) responseMsg.data());
  }
  
  return false;
}
