// NB: http://www.cplusplus.com/reference/ctime/time/ is helpful here
#include <deque>
#include <time.h>
#include <mutex>

#include "quota_tracker.h"
using namespace std;

/// quota_tracker::Internal is the class that stores all the members of a
/// quota_tracker object. To avoid pulling too much into the .h file, we are
/// using the PIMPL pattern
/// (https://www.geeksforgeeks.org/pimpl-idiom-in-c-with-examples/)
struct quota_tracker::Internal {
  /// An event is a timestamped amount.  We don't care what the amount
  /// represents, because the code below will only sum the amounts in a
  /// collection of events and compare it against a quota.
  struct event {
    /// The time at which the request was made
    time_t when;

    /// The amount of resource consumed at the above time
    size_t amnt;
  };

  deque <event> eventDeck;
  size_t amount2;   //maximum amount of service
  double duration2; 
  mutex lock;
  /// Construct the Internal object
  ///
  /// @param amount   The maximum amount of service
  /// @param duration The time during the service maximum can be spread out
  Internal(size_t amount, double duration) {
    amount2 = amount;
    duration2 = duration;
  }
};

/// Construct an object that limits usage to quota_amount per quota_duration
/// seconds
///
/// @param amount   The maximum amount of service
/// @param duration The time during the service maximum can be spread out
quota_tracker::quota_tracker(size_t amount, double duration)
    : fields(new Internal(amount, duration)) {}

/// Construct a quota_tracker from another quota_tracker
///
/// @param other The quota tracker to use to build a new quota tracker
quota_tracker::quota_tracker(const quota_tracker &other) : fields() {
  // TODO: You'll want to figure out how to make a copy constructor for this
  /*
  amount2 = other.amount2;
  duration2 = other.duration2;
  */
}


/// Destruct a quota tracker
quota_tracker::~quota_tracker() = default;

/// Decides if a new event is permitted.  The attempt is allowed if it could
/// be added to events, while ensuring that the sum of amounts for all events
/// with (time > now-q_dur), is less than q_amnt.
///
/// @param amount The amount of the new request
///
/// @returns True if the amount could be added without violating the quota
bool quota_tracker::check(size_t amount) { 

  //lock the quota_tracker
  (fields -> lock).lock();

  size_t total = 0;
  for (size_t i =0; i < fields -> eventDeck.size(); i++){
    if (((fields -> eventDeck).at(i).when) > (time(NULL) - (fields -> duration2)))
      total += (fields -> eventDeck).at(i).amnt;
  }

  //unlock the quota_tracker
  (fields -> lock).unlock();

  //if what we have done + what we will do <= what we are allowed to do
  if ((total + amount) <= (fields -> amount2))
    return true;
  //else return false
  return false; 
}

/// Actually add a new event to the quota tracker
void quota_tracker::add(size_t amount) {
  //lock the quota_tracker
  (fields -> lock).lock();
  
  Internal::event event0;
  event0.when = time(NULL);
  event0.amnt = amount;
  fields -> eventDeck.push_front(event0);

  //unlock the quota_tracker
  (fields -> lock).unlock();
}