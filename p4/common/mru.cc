#include <deque>
#include <mutex>
#include <iostream>

#include "mru.h"
#include "file.h"

using namespace std;

/// mru_manager::Internal is the class that stores all the members of a
/// mru_manager object. To avoid pulling too much into the .h file, we are using
/// the PIMPL pattern
/// (https://www.geeksforgeeks.org/pimpl-idiom-in-c-with-examples/)
struct mru_manager::Internal {
 
  size_t elements2;
  deque <string> deck;
  mutex lock;
  /// Construct the Internal object by setting the fields that are
  /// user-specified
  ///
  /// @param elements The number of elements that can be tracked
  Internal(size_t elements) {
    elements2 = elements;
  }
};

/// Construct the mru_manager by specifying how many things it should track
mru_manager::mru_manager(size_t elements) : fields(new Internal(elements)) {}

/// Destruct an mru_manager
mru_manager::~mru_manager() = default;


/// Insert an element into the mru_manager, making sure that (a) there are no
/// duplicates, and (b) the manager holds no more than /max_size/ elements.
///
/// @param elt The element to insert
void mru_manager::insert(const string &elt) {
  //lock the mru
  (fields -> lock).lock();

  //if matching, remove the match
  for (size_t i = 0; i < fields -> deck.size(); i++) {
    if (fields -> deck.at(i).compare(elt) == 0){
        fields -> deck.erase(fields->deck.begin() + i);
    }
  }

  //if at max push the back off
  if (fields -> deck.size() >= fields -> elements2){
    fields -> deck.pop_back();
  }

  //put the element onto the mru
  fields -> deck.push_front(elt);
  
  //ulock the mru
  (fields -> lock).unlock();
  return;
}

/// Remove an instance of an element from the mru_manager.  This can leave the
/// manager in a state where it has fewer than max_size elements in it.
///
/// @param elt The element to remove
void mru_manager::remove(const string &elt) {
  //lock the mru
  (fields -> lock).lock();

  for (size_t i = 0; i < fields -> deck.size(); i++) {
    if (fields -> deck.at(i).compare(elt) == 0){
      fields -> deck.erase(fields->deck.begin() + i);
    }
  }

  //ulock the mru
  (fields -> lock).unlock();
  return;
}

/// Clear the mru_manager
void mru_manager::clear() {
  (fields -> deck).clear();
}

/// Produce a concatenation of the top entries, in order of popularity
///
/// @returns A newline-separated list of values
string mru_manager::get() {

  //lock the mru
  (fields -> lock).lock();
  string top = ""; 

  for (size_t i = 0; i < fields -> deck.size(); i++) {
    top.append(fields -> deck.at(i));
    top.append("\n");
  }

  //ulock the mru
  (fields -> lock).unlock();

  return top; 
  
};