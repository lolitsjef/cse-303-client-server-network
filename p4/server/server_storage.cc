#include <cstdio>
#include <iostream>
#include <openssl/md5.h>
#include <unistd.h>
#include <unordered_map>

#include "../common/contextmanager.h"
#include "../common/err.h"
#include "../common/hashtable.h"
#include "../common/mru.h"
#include "../common/protocol.h"
#include "../common/vec.h"
#include "../common/file.h"

#include "server_authtableentry.h"
#include "server_persist.h"
#include "server_quotas.h"
#include "server_storage.h"

using namespace std;
//p4p4p4p4
/// Storage::Internal is the private struct that holds all of the fields of the
/// Storage object.  Organizing the fields as an Internal is part of the PIMPL
/// pattern.
struct Storage::Internal {
  /// A unique 8-byte code to use as a prefix each time an AuthTable Entry is
  /// written to disk.
  inline static const string AUTHENTRY = "AUTHAUTH";

  /// A unique 8-byte code to use as a prefix each time a KV pair is written to
  /// disk.
  inline static const string KVENTRY = "KVKVKVKV";

  /// A unique 8-byte code for incremental persistence of changes to the auth
  /// table
  inline static const string AUTHDIFF = "AUTHDIFF";

  /// A unique 8-byte code for incremental persistence of updates to the kv
  /// store
  inline static const string KVUPDATE = "KVUPDATE";

  /// A unique 8-byte code for incremental persistence of deletes to the kv
  /// store
  inline static const string KVDELETE = "KVDELETE";

  /// The map of authentication information, indexed by username
  ConcurrentHashTable<string, AuthTableEntry> auth_table;

  /// The map of key/value pairs
  ConcurrentHashTable<string, vec> kv_store;

  /// filename is the name of the file from which the Storage object was loaded,
  /// and to which we persist the Storage object every time it changes
  string filename = "";

  /// The open file
  FILE *storage_file = nullptr;

  /// The upload quota
  const size_t up_quota;

  /// The download quota
  const size_t down_quota;

  /// The requests quota
  const size_t req_quota;

  /// The number of seconds over which quotas are enforced
  const double quota_dur;

  /// The MRU table for tracking the most recently used keys
  mru_manager mru;

  /// A table for tracking quotas
  ConcurrentHashTable<string, Quotas *> quota_table;

  /// Construct the Storage::Internal object by setting the filename and bucket
  /// count
  ///
  /// @param fname       The name of the file that should be used to load/store
  ///                    the data
  /// @param num_buckets The number of buckets for the hash
  Internal(string fname, size_t num_buckets, size_t upq, size_t dnq, size_t rqq,
           double qd, size_t top)
      : auth_table(num_buckets), kv_store(num_buckets), filename(fname),
        up_quota(upq), down_quota(dnq), req_quota(rqq), quota_dur(qd), mru(top),
        quota_table(num_buckets) {}

  // NB: You may want to add a function here for interacting with the quota
  // table to check a user's quota
};

/// Construct an empty object and specify the file from which it should be
/// loaded.  To avoid exceptions and errors in the constructor, the act of
/// loading data is separate from construction.
///
/// @param fname       The name of the file that should be used to load/store
///                    the data
/// @param num_buckets The number of buckets for the hash
Storage::Storage(const string &fname, size_t num_buckets, size_t upq,
                 size_t dnq, size_t rqq, double qd, size_t top)
    : fields(new Internal(fname, num_buckets, upq, dnq, rqq, qd, top)) {}

/// Destructor for the storage object.
///
/// NB: The compiler doesn't know that it can create the default destructor in
///     the .h file, because PIMPL prevents it from knowing the size of
///     Storage::Internal.  Now that we have reified Storage::Internal, the
///     compiler can make a destructor for us.
Storage::~Storage() = default;


//helper function to update the file
bool append_file(const string &filename, const char *data, size_t bytes) {
  FILE *f = fopen(filename.c_str(), "a");
  if (f == nullptr) {
    cerr << "Unable to open '" << filename << "' for writing\n";
    return false;
  }
  ContextManager closer([&]() { fclose(f); }); // close file when we return

  // NB: since we know it's a true file, we don't need to worry about short
  //     counts and EINTR.
  if (fwrite(data, sizeof(char), bytes, f) != bytes) {
    cerr << "Incorrect number of bytes written to " << filename << endl;
    return false;
  }
  return true;
}

/// Populate the Storage object by loading this.filename.  Note that load()
/// begins by clearing the maps, so that when the call is complete, exactly and
/// only the contents of the file are in the Storage object.
///
/// @returns false if any error is encountered in the file, and true otherwise.
///          Note that a non-existent file is not an error.
bool Storage::load() {
  FILE *f = fopen(fields->filename.c_str(), "r");
  if (f == nullptr) {
    cerr << "File not found: " << fields->filename << endl;
    return true;
  }

  ContextManager fileCloser([&] () { fclose(f); });
  vec fileStuff;
  fileStuff = load_entire_file((fields -> filename));
  cerr << "Loaded: " << (fields -> filename) << endl;

  size_t index = 0;   //current place in the file vector

  while(true){
    string entryType;  //KVKVKVKV or AUTHAUTH or AUTHDIFF or KVUPDATE or KVDELETE
    size_t keySize; //SIZE of Key
    string key;        //string of the key
    size_t valSize; //SIZE of val
    vec val;           //vector of the val

    size_t userSize; //SIZE of username
    string user;        //string of the username
    size_t passSize; //SIZE of password
    string pass;        //string of the password
    size_t conSize;  //SIZE of content
    vec con;         //vector of the content

    //populate entry type to KVKVKVKV or AUTHAUTH or AUTHDIFF or KVUPDATE or KVDELETE
    for (size_t i = 0; i < 8; i++){
      entryType+= (char)(fileStuff.at(index++));
    }

    //if a KV entry
    if (entryType == "KVKVKVKV"){

      //get the key
      int a = fileStuff.at(index++);
      int b = fileStuff.at(index++) << 8;
      int c = fileStuff.at(index++) << 16;
      int d = fileStuff.at(index++) << 24;
      keySize = a|b|c|d;

      for (size_t i = 0; i < keySize; i++){
        key+= (char)(fileStuff.at(index++));
      }

      //get the val
      a = fileStuff.at(index++);
      b = fileStuff.at(index++) << 8;
      c = fileStuff.at(index++) << 16;
      d = fileStuff.at(index++) << 24;
      valSize = a|b|c|d;

      for (size_t i = 0; i < valSize; i++){
        val.push_back(fileStuff.at(index++));
      }

      //insert the key and the value into kv_store
      auto nothing = [&] () {};
      fields -> kv_store.insert(key, val, nothing);
    }

    //if an authauth
    else if (entryType == "AUTHAUTH"){ 

      //get the username
      int a = fileStuff.at(index++);
      int b = fileStuff.at(index++) << 8;
      int c = fileStuff.at(index++) << 16;
      int d = fileStuff.at(index++) << 24;
      userSize = a|b|c|d;
      
      for (size_t i = 0; i < userSize; i++){
        user+= (char)fileStuff.at(index++);
      }

      //get the password
      a = fileStuff.at(index++);
      b = fileStuff.at(index++) << 8;
      c = fileStuff.at(index++) << 16;
      d = fileStuff.at(index++) << 24;
      passSize = a|b|c|d;

      for (size_t i = 0; i < passSize; i++){
        pass+= (char)fileStuff.at(index++);
      }

      //get the content
      a = fileStuff.at(index++);
      b = fileStuff.at(index++) << 8;
      c = fileStuff.at(index++) << 16;
      d = fileStuff.at(index++) << 24;
      conSize = a|b|c|d;

      for (size_t i = 0; i < conSize; i++){
        con.push_back(fileStuff.at(index++));
      }

      AuthTableEntry entry;
      entry.username = user;
      entry.pass_hash = pass;
      entry.content = con;

      //insert the key and the value into auth_table
      auto nothing = [&] () {};
      fields -> auth_table.insert(user, entry, nothing);

    }

    //if an auth diff
    else if (entryType == "AUTHDIFF"){ 
      //get the username
      int a = fileStuff.at(index++);
      int b = fileStuff.at(index++) << 8;
      int c = fileStuff.at(index++) << 16;
      int d = fileStuff.at(index++) << 24;
      userSize = a|b|c|d;
      
      for (size_t i = 0; i < userSize; i++){
        user+= (char)fileStuff.at(index++);
      }

      //get the content
      a = fileStuff.at(index++);
      b = fileStuff.at(index++) << 8;
      c = fileStuff.at(index++) << 16;
      d = fileStuff.at(index++) << 24;
      conSize = a|b|c|d;

      for (size_t i = 0; i < conSize; i++){
        con.push_back(fileStuff.at(index++));
      }

      //update value into auth_table
      auto updateContent = [&] (auto &pastEntry) {
        pastEntry.content = con;
      };
      
      (fields -> auth_table).do_with(user, updateContent);

    }
    // if entry is KVUPDATE
    else if (entryType == "KVUPDATE"){
      //get the key
      int a = fileStuff.at(index++);
      int b = fileStuff.at(index++) << 8;
      int c = fileStuff.at(index++) << 16;
      int d = fileStuff.at(index++) << 24;
      keySize = a|b|c|d;
      
      for (size_t i = 0; i < keySize; i++){
        key+= (char)(fileStuff.at(index++));
      }

      //get the val
      a = fileStuff.at(index++);
      b = fileStuff.at(index++) << 8;
      c = fileStuff.at(index++) << 16;
      d = fileStuff.at(index++) << 24;
      valSize = a|b|c|d;

      for (size_t i = 0; i < valSize; i++){
        val.push_back(fileStuff.at(index++));
      }

      //update the key and the value into kv_store
      auto nothing = [&] () {};
      auto nothing2 = [&] () {};
      fields -> kv_store.upsert(key, val, nothing, nothing2);
    }
    //if entry is KVDELETE
    else if (entryType == "KVDELETE"){

      //get the key
      int a = fileStuff.at(index++);
      int b = fileStuff.at(index++) << 8;
      int c = fileStuff.at(index++) << 16;
      int d = fileStuff.at(index++) << 24;
      keySize = a|b|c|d;
      
      for (size_t i = 0; i < keySize; i++){
        key+= (char)(fileStuff.at(index++));
      }

      //remove the key and the value from kv_store
      auto nothing = [&] () {};
      fields -> kv_store.remove(key, nothing);
    }

    //would signify entry not starting with AUTHAUTH, KVKVKVKV, etc...
    else {
      cerr << "Error parsing: " << fields->filename << endl;
      return false;
    }

    //checking for EOF
    if (index >= fileStuff.size())
      break;
  }
  return true;
}


/// Create a new entry in the Auth table.  If the user_name already exists, we
/// should return an error.  Otherwise, hash the password, and then save an
/// entry with the username, hashed password, and a zero-byte content.
///
/// @param user_name The user name to register
/// @param pass      The password to associate with that user name
///
/// @returns False if the username already exists, true otherwise
bool Storage::add_user(const string &user_name, const string &pass) {

  //create a new auth table entry
  AuthTableEntry newUser;
  newUser.username = user_name;
  unsigned char* hashedPass = NULL;
  hashedPass = MD5((unsigned char*) pass.data(), pass.length(), NULL);
  string hashedPassString(reinterpret_cast<char*>(hashedPass));
  newUser.pass_hash = hashedPassString;

  auto printAuthStuff = [&] (){
    vec authStuff;
    vec_append(authStuff, (fields -> AUTHENTRY));
    vec_append(authStuff, newUser.username.size());
    vec_append(authStuff, newUser.username);
    vec_append(authStuff, newUser.pass_hash.size());
    vec_append(authStuff, newUser.pass_hash);
    vec_append(authStuff, newUser.content.size());
    vec_append(authStuff, newUser.content);

    //append_file is a helper function we created on line 93
    append_file(fields->filename.c_str(),(const char*)authStuff.data(), authStuff.size());
    
  };

  auto nothing = [] () {};

  //create quotas for user
  Quotas* userQuotas = new Quotas({quota_tracker (fields -> up_quota, fields -> quota_dur), quota_tracker (fields -> down_quota, fields -> quota_dur), quota_tracker (fields -> req_quota, fields -> quota_dur)});

  //insert the auth table entry
  if (fields -> auth_table.insert(user_name, newUser, printAuthStuff)){
    fields -> quota_table.insert(user_name, userQuotas, nothing);
    return true;
  }

  return false;
}

/// Set the data bytes for a user, but do so if and only if the password
/// matches
///
/// @param user_name The name of the user whose content is being set
/// @param pass      The password for the user, used to authenticate
/// @param content   The data to set for this user
///
/// @returns A pair with a bool to indicate error, and a vector indicating the
///          message (possibly an error message) that is the result of the
///          attempt
vec Storage::set_user_data(const string &user_name, const string &pass, const vec &content) {
  
 //check if user exists and if they authenticated
  if (!auth(user_name, pass))
    return vec_from_string(RES_ERR_LOGIN);

  auto updateData = [&] (AuthTableEntry userData) {
    userData.content = content;    

    vec authStuff;
    vec_append(authStuff, (fields -> AUTHDIFF));
    vec_append(authStuff, userData.username.size());
    vec_append(authStuff, userData.username);
    vec_append(authStuff, userData.content.size());
    vec_append(authStuff, userData.content);

    append_file(fields->filename.c_str(),(const char*)authStuff.data(), authStuff.size());
  
  };  

  //will return false if no user currently exists
  if (!((fields -> auth_table).do_with(user_name, updateData)))
    return vec_from_string(RES_ERR_NO_USER);
  
  //return ok if they authenticated
  return vec_from_string(RES_OK);
}

/// Return a copy of the user data for a user, but do so only if the password
/// matches
///
/// @param user_name The name of the user who made the request
/// @param pass      The password for the user, used to authenticate
/// @param who       The name of the user whose content is being fetched
///
/// @returns A pair with a bool to indicate error, and a vector indicating the
///          data (possibly an error message) that is the result of the
///          attempt.  Note that "no data" is an error
pair<bool, vec> Storage::get_user_data(const string &user_name, const string &pass, const string &who) {

  //authenticate the user
  if (!auth(user_name, pass))
    return {true, vec_from_string(RES_ERR_LOGIN)}; 

  vec whoContent;
  //read the content
  auto getData = [&] (auto userData) {
    whoContent = userData.content;
  };   

  //if who doesnt exist return error                          
  if (!((fields -> auth_table).do_with_readonly(who, getData)))
    return {true, vec_from_string(RES_ERR_NO_USER)};  
  
  //if the content we're updating is empty
  if (whoContent.size() == 0)
    return  {true, vec_from_string(RES_ERR_NO_DATA)};

  return {false, whoContent};
}



/// Return a newline-delimited string containing all of the usernames in the
/// auth table
///
/// @param user_name The name of the user who made the request
/// @param pass      The password for the user, used to authenticate
///
/// @returns A vector with the data, or a vector with an error message
pair<bool, vec> Storage::get_all_users(const string &user_name, const string &pass) {
  //authenticate user
  if (!auth(user_name, pass))
    return {true, vec_from_string(RES_ERR_LOGIN)};      

  vec allStuff;
  auto getAllStuff = [&] (string user, AuthTableEntry entry) {
    vec_append(allStuff, entry.username);
    allStuff.push_back('\n');
  };  

  auto then = [] () {
  }; 

  //if user doesnt exist or doesnt authenticate then return an error
  (fields -> auth_table).do_all_readonly(getAllStuff, then);
  if (allStuff.size() == 0)
    return {true, vec_from_string(RES_ERR_NO_DATA)};
  return {false, allStuff};
  
}

/// Authenticate a user
///
/// @param user_name The name of the user who made the request
/// @param pass      The password for the user, used to authenticate
///
/// @returns True if the user and password are valid, false otherwise
bool Storage::auth(const string &user_name, const string &pass) {

  //hash the pass and cast as string
  unsigned char* hashedPass = NULL;
  hashedPass = MD5((unsigned char*) pass.data(), pass.length(), NULL);
  string hashedPassString(reinterpret_cast<char*>(hashedPass));

  bool auth = false;
  //compare hashed passwords to authenticate the user
  auto authorizeUser = [&] (const AuthTableEntry userData) {
    if (userData.pass_hash == hashedPassString){
      auth = true;
    }
  };  

  //if user doesnt exist or doesnt authenticate then return true/false
  (fields -> auth_table).do_with(user_name, authorizeUser);
  return auth;
}


/// Write the entire Storage object to the file specified by this.filename.
/// To ensure durability, Storage must be persisted in two steps.  First, it
/// must be written to a temporary file (this.filename.tmp).  Then the
/// temporary file can be renamed to replace the older version of the Storage
/// object.
void Storage::persist() {


    string tempfile = "tempfile.tmp";
    vec KVstuff;
    vec authStuff;


  auto printKVStuff = [&] (const string key, const vec value) {
    vec_append(KVstuff, (fields -> KVENTRY));
    vec_append(KVstuff, key.size());
    vec_append(KVstuff, key);
    vec_append(KVstuff, value.size());
    vec_append(KVstuff, value); 
  };

  //print auth table
  auto printAuthStuff = [&] (const string key, AuthTableEntry userEntry){
    //make vec of stuff to print for each auth table entry
    vec_append(authStuff, (fields -> AUTHENTRY));
    vec_append(authStuff, userEntry.username.size());
    vec_append(authStuff, userEntry.username);
    vec_append(authStuff, userEntry.pass_hash.size());
    vec_append(authStuff, userEntry.pass_hash);
    vec_append(authStuff, userEntry.content.size());
    vec_append(authStuff, userEntry.content);
  };

  //rename the file
  auto nothing = [&] () {};

  //calls the write function and then calls the renamefile function
  auto doKVwrite = [&] (){
  (fields -> kv_store).do_all_readonly(printKVStuff, nothing);
  }; 

  //start lambda chain by printing auth table and then calling kvwrite
  (fields -> auth_table).do_all_readonly(printAuthStuff, doKVwrite);

  //append the KV and authtable
  vec_append(KVstuff, authStuff);

  //write them
  write_file(tempfile, (const char*) KVstuff.data(), KVstuff.size());

  //final renaming to company.dir
  rename(tempfile.c_str(), (fields -> filename).c_str());

}

/// Close any open files related to incremental persistence
///
/// NB: this cannot be called until all threads have stopped accessing the
///     Storage object
void Storage::shutdown() {}

/// Create a new key/value mapping in the table
///
/// @param user_name The name of the user who made the request
/// @param pass      The password for the user, used to authenticate
/// @param key       The key whose mapping is being created
/// @param val       The value to copy into the map
///
/// @returns A vec with the result message
vec Storage::kv_insert(const string &user_name, const string &pass, const string &key, const vec &val) {
  if (!auth(user_name, pass))
    return vec_from_string(RES_ERR_LOGIN); 

  
  vec check = vec_from_string(RES_OK);

  auto checkQuota = [&] (Quotas* userQuotas) {
    if (!userQuotas -> uploads.check(val.size()))
      check = vec_from_string(RES_ERR_QUOTA_UP);
    else
      userQuotas -> uploads.add(val.size());

    if (!userQuotas -> requests.check(1))
      check = vec_from_string(RES_ERR_QUOTA_REQ);
    else
      userQuotas -> requests.add(1);

  };
  //check and update quotas
  fields -> quota_table.do_with(user_name, checkQuota);
  if (check != vec_from_string(RES_OK))
    return check;
  
  //writes KV pair to file
  auto printKVStuff = [&] (){
    vec kvStuff;
    vec_append(kvStuff, (fields -> KVENTRY));
    vec_append(kvStuff, key.size());
    vec_append(kvStuff, key);
    vec_append(kvStuff, val.size());
    vec_append(kvStuff, val);
    
    append_file(fields->filename.c_str(),(const char*)kvStuff.data(), kvStuff.size());
  };

  //checking to ensure key doesn't already exist
  if (!fields -> kv_store.insert(key, val, printKVStuff))
    return vec_from_string(RES_ERR_KEY);
  
  fields -> mru.insert(key);
  return vec_from_string(RES_OK);
};


/// Get a copy of the value to which a key is mapped
///
/// @param user_name The name of the user who made the request
/// @param pass      The password for the user, used to authenticate
/// @param key       The key whose value is being fetched
///
/// @returns A pair with a bool to indicate error, and a vector indicating the
///          data (possibly an error message) that is the result of the
///          attempt.
pair<bool, vec> Storage::kv_get(const string &user_name, const string &pass, const string &key) {
  //authenticate user
  if (!auth(user_name, pass))
    return {true, vec_from_string(RES_ERR_LOGIN)};      

  //get the content
  vec content;
  auto getData = [&] (vec value) {
      content = value;    
  };   

  //key doesn't exist                       
  if (!((fields -> kv_store).do_with_readonly(key, getData)))
    return {true, vec_from_string(RES_ERR_KEY)}; 


  //if value associated with key is empty    
  if (content.size() == 0)
    return  {true, vec_from_string(RES_ERR_NO_DATA)};
  
  //check quotas
  vec check = vec_from_string(RES_OK);
  auto checkQuota = [&] (Quotas* userQuotas) {
    if (!userQuotas -> downloads.check(content.size()))
      check = vec_from_string(RES_ERR_QUOTA_DOWN);
    else
      userQuotas -> downloads.add(content.size());

    if (!userQuotas -> requests.check(1))
      check = vec_from_string(RES_ERR_QUOTA_REQ);
    else
      userQuotas -> requests.add(1);
  };
  //check and update quotas
  fields -> quota_table.do_with(user_name, checkQuota);
  if (check != vec_from_string(RES_OK))
    return {true, check};

  fields -> mru.insert(key);
  return {false, content};
};

/// Delete a key/value mapping
///
/// @param user_name The name of the user who made the request
/// @param pass      The password for the user, used to authenticate
/// @param key       The key whose value is being deleted
///
/// @returns A vec with the result message
vec Storage::kv_delete(const string &user_name, const string &pass, const string &key) {
  //authenticate user
  if (!auth(user_name, pass))
    return vec_from_string(RES_ERR_LOGIN); 

  vec check = vec_from_string(RES_OK);

  auto checkQuota = [&] (Quotas* userQuotas) {
        if (!userQuotas -> requests.check(1))
      check = vec_from_string(RES_ERR_QUOTA_REQ);
    else
      userQuotas -> requests.add(1);
  };
  //check and update quotas
  fields -> quota_table.do_with(user_name, checkQuota);
  if (check != vec_from_string(RES_OK))
    return check;

  //write to remove KV pair from the file
  auto removeKVStuff = [&] (){
    vec kvStuff;
    vec_append(kvStuff, (fields -> KVDELETE));
    vec_append(kvStuff, key.size());
    vec_append(kvStuff, key);
    
    append_file(fields->filename.c_str(),(const char*)kvStuff.data(), kvStuff.size());
    
  };

  //chceks to ensure key exists that we're removing
  if (!fields -> kv_store.remove(key, removeKVStuff)){
    return vec_from_string(RES_ERR_KEY);
  }

  fields -> mru.remove(key);

  return vec_from_string(RES_OK);
};

/// Insert or update, so that the given key is mapped to the give value
///
/// @param user_name The name of the user who made the request
/// @param pass      The password for the user, used to authenticate
/// @param key       The key whose mapping is being upserted
/// @param val       The value to copy into the map
///
/// @returns A vec with the result message.  Note that there are two "OK"
///          messages, depending on whether we get an insert or an update.
vec Storage::kv_upsert(const string &user_name, const string &pass, const string &key, const vec &val) {
  if (!auth(user_name, pass))
    return vec_from_string(RES_ERR_LOGIN); 

  vec check = vec_from_string(RES_OK);

  auto checkQuota = [&] (Quotas* userQuotas) {
    if (!userQuotas -> uploads.check(val.size()))
      check = vec_from_string(RES_ERR_QUOTA_UP);
    else
      userQuotas -> uploads.add(val.size());

    if (!userQuotas -> requests.check(1))
      check = vec_from_string(RES_ERR_QUOTA_REQ);
    else
      userQuotas -> requests.add(1);

  };
  //check and update quotas
  fields -> quota_table.do_with(user_name, checkQuota);
  if (check != vec_from_string(RES_OK))
    return check;
  
  //lambda in the case of a key not already existing
  auto onIns = [&] (){
    vec kvStuff;
    vec_append(kvStuff, (fields -> KVENTRY));
    vec_append(kvStuff, key.size());
    vec_append(kvStuff, key);
    vec_append(kvStuff, val.size());
    vec_append(kvStuff, val);
    
    append_file(fields->filename.c_str(),(const char*)kvStuff.data(), kvStuff.size());
    
  };
  
  //lambda in the case of key existing, replacing the value
  auto onUpd = [&] (){
    vec kvStuff;
    vec_append(kvStuff, (fields -> KVUPDATE));
    vec_append(kvStuff, key.size());
    vec_append(kvStuff, key);
    vec_append(kvStuff, val.size());
    vec_append(kvStuff, val);
    
    append_file(fields->filename.c_str(),(const char*)kvStuff.data(), kvStuff.size());
    
  };

  fields -> mru.remove(key);

  //specifies if we've done an insertion or update
  if (!fields -> kv_store.upsert(key, val, onIns, onUpd))
    return vec_from_string("OKUPD");
  return vec_from_string("OKINS");
};

/// Return all of the keys in the kv_store, as a "\n"-delimited string
///
/// @param user_name The name of the user who made the request
/// @param pass      The password for the user, used to authenticate
///
/// @returns A pair with a bool to indicate errors, and a vec with the result
///          (possibly an error message).
pair<bool, vec> Storage::kv_all(const string &user_name, const string &pass) {

  //authenticate user
  if (!auth(user_name, pass))
    return {true, vec_from_string(RES_ERR_LOGIN)};      
  
  vec allStuff;
  auto getAllStuff = [&] (string key, vec value) {
    vec_append(allStuff, key);
    allStuff.push_back('\n');
  };  

  auto then = [] () {
  }; 

  //if user doesnt exist or doesnt authenticate then return an error
  (fields -> kv_store).do_all_readonly(getAllStuff, then);
  if (allStuff.size() == 0)
    return {true, vec_from_string(RES_ERR_NO_DATA)};
  
  //check quotas
  vec check = vec_from_string(RES_OK);
  auto checkQuota = [&] (Quotas* userQuotas) {
    if (!userQuotas -> downloads.check(allStuff.size()))
      check = vec_from_string(RES_ERR_QUOTA_DOWN);
    else
      userQuotas -> downloads.add(allStuff.size());

    if (!userQuotas -> requests.check(1))
      check = vec_from_string(RES_ERR_QUOTA_REQ);
    else
      userQuotas -> requests.add(1);
  };
  //check and update quotas
  fields -> quota_table.do_with(user_name, checkQuota);
  if (check != vec_from_string(RES_OK))
    return {true, check};

  return {false, allStuff};
};

/// Return all of the keys in the kv_store's MRU cache, as a "\n"-delimited
/// string
///
/// @param user_name The name of the user who made the request
/// @param pass      The password for the user, used to authenticate
///
/// @returns A pair with a bool to indicate errors, and a vec with the result
///          (possibly an error message).
pair<bool, vec> Storage::kv_top(const string &user_name, const string &pass) {
  

  if (!auth(user_name, pass)) 
    return {true, vec_from_string(RES_ERR_LOGIN)};

  string topString = fields -> mru.get();
//check quotas
  vec check = vec_from_string(RES_OK);
  auto checkQuota = [&] (Quotas* userQuotas) {
    if (!userQuotas -> downloads.check(topString.size()))
      check = vec_from_string(RES_ERR_QUOTA_DOWN);
    else
      userQuotas -> downloads.add(topString.size());

    if (!userQuotas -> requests.check(1))
      check = vec_from_string(RES_ERR_QUOTA_REQ);
    else
      userQuotas -> requests.add(1);
  };
  //check and update quotas
  fields -> quota_table.do_with(user_name, checkQuota);
  if (check != vec_from_string(RES_OK))
    return {true, check};

  if (topString.size() > 0)
    return {false, vec_from_string(topString) };

  return {true, vec_from_string(RES_ERR_NO_DATA)};
};


//string Storage::quotaCheck(int cmd, )
