#include <atomic>
#include <dlfcn.h>
#include <iostream>
#include <map>
#include <mutex>
#include <shared_mutex>
#include <string>
#include <sys/wait.h>
#include <unistd.h>
#include <utility>
#include <vector>

#include "../common/contextmanager.h"
#include "../common/file.h"
#include "../common/functypes.h"
#include "../common/protocol.h"
#include "../common/vec.h"

#include "func_table.h"

using namespace std;

/// func_table::Internal is the private struct that holds all of the fields of
/// the func_table object.  Organizing the fields as an Internal is part of the
/// PIMPL pattern.
///
/// Among other things, this struct will probably need to have a map of loaded
/// functions and a shared_mutex.  The map will probably need to hold some kind
/// of struct that is able to support graceful shutdown, as well as the
/// association of names to map/reduce functions
struct func_table::Internal {
  map<string, pair<map_func, reduce_func>> fmap;  //map of mrname and pair<map(),reduce()>
  shared_mutex mutLock;                            //protecting the data
  vector<pair<void*, string>> handles;             //handles and filenames
  atomic<int> fileCounter = 0;
};

/// Construct a function table for storing registered functions
func_table::func_table() : fields(new Internal()) {}

/// Destruct a function table
func_table::~func_table() = default;

/// Register the map() and reduce() functions from the provided .so, and
/// associate them with the provided name.
///
/// @param mrname The name to associate with the functions
/// @param so     The so contents from which to find the functions
///
/// @returns a vec with a status message
vec func_table::register_mr(const string &mrname, const vec &so) {
  string filename = "./functionFile" + to_string(fields -> fileCounter++) +".so";
  if (!write_file(filename, (const char*) so.data(), so.size()))          //if issue writing file
    return vec_from_string(RES_ERR_SO);

  //load .so file
  void *handle;
  handle = dlopen(filename.c_str(), RTLD_LAZY);                          
  if (!handle)                                                          //if issue opening .so file
    return vec_from_string(RES_ERR_SO);
                     

  //load map and function
  map_func m = (map_func) dlsym(handle, "map");
  if (dlerror() != NULL){
    dlclose(handle);
    return vec_from_string(RES_ERR_SO);
  }

  //load reduce function
  reduce_func r = (reduce_func) dlsym(handle, "reduce");
  if (dlerror() != NULL){
    dlclose(handle);
    return vec_from_string(RES_ERR_SO);
  }

  //lock
  unique_lock lock(fields -> mutLock);
  
  //check to see if mrname is registered in the map already
  if (fields -> fmap.find(mrname) != fields -> fmap.end())
    return vec_from_string(RES_ERR_FUNC);

  //make pair to put onto fmap
  pair<map_func, reduce_func> funcs;
  funcs.first = m;
  funcs.second = r;
  //put functions on the map
  fields -> fmap.insert({mrname, funcs});
  //put handle onto vector of handles
  fields -> handles.push_back({handle, filename});  
  return vec_from_string(RES_OK);
}

/// Get the (already-registered) map() and reduce() functions asssociated with
/// a name.
///
/// @param name The name with which the functions were mapped
///
/// @returns A pair of function pointers, or {nullptr, nullptr} on error
pair<map_func, reduce_func> func_table::get_mr(const string &mrname) {
  //lock
  shared_lock lock(fields -> mutLock);
  //declare iterator
  map<string, pair<map_func, reduce_func>>::iterator it;

  //find returns iterator if there or returns end if not there
  it = fields -> fmap.find(mrname);
  if (it == fields -> fmap.end())
    return {nullptr, nullptr};

  //if in the map return the values
  return fields -> fmap.find(mrname) -> second;
}

/// When the function table shuts down, we need to de-register all the .so
/// files that were loaded.
void func_table::shutdown() {
  //lock
  unique_lock lock(fields -> mutLock);

  //iterate through functable and dlclose everything and delete the files
  for (size_t i = 0; i < fields -> handles.size(); i++){
    dlclose(fields -> handles.at(i).first);
    remove (fields -> handles.at(i).second.c_str());
  }
  
}