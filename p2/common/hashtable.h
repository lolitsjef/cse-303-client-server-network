#pragma once

#include <atomic>
#include <functional>
#include <mutex>
#include <thread>
#include <utility>
#include <vector>

/// ConcurrentHashTable is a concurrent hash table (a Key/Value store).  It is
/// not resizable, which means that the O(1) guarantees of a hash table are lost
/// if the number of elements in the table gets too big.
///
/// The ConcurrentHashTable is templated on the Key and Value types
///
/// The general structure of the ConcurrentHashTable is that we have an array of
/// buckets.  Each bucket has a mutex and a vector of entries.  Each entry is a
/// pair, consisting of a key and a value.  We can use std::hash() to choose a
/// bucket from a key.
using namespace std;

template <typename K, typename V> class ConcurrentHashTable {

  struct ht_bucket{
      vector<pair<K,V>> elements;
      mutex bucketLock;
  };

public:
  int num_buckets;
  vector<ht_bucket> hashTable;

  /// Construct a concurrent hash table by specifying the number of buckets it
  /// should have
  ///
  /// @param _buckets The number of buckets in the concurrent hash table
  ConcurrentHashTable(size_t _buckets) {
    num_buckets = _buckets;
    hashTable = vector<ht_bucket> (_buckets);
  }

  /// Clear the Concurrent Hash Table.  This operation needs to use 2pl
  void clear() {
    //growing phase, aquire locks
    for (int i = 0; i < num_buckets; i ++){
      (hashTable.at(i).bucketLock).lock();
      //clear what is in the bucket
      for (int k = 0; k < hashTable.at(i).size(); k++){
        (hashTable.at(i).elements).pop_back();
      }
    }
    //shrinking phase, release locks
    for (int i = 0; i < num_buckets; i ++){
      (hashTable.at(i).bucketLock).unlock();
    }
  }

  /// Insert the provided key/value pair only if there is no mapping for the key
  /// yet.
  ///
  /// @param key The key to insert
  /// @param val The value to insert
  ///
  /// @returns true if the key/value was inserted, false if the key already
  /// existed in the table
  bool insert(K key, V val) { 
    //define the pair
    pair<K,V> thing;
    thing.first = key;
    thing.second = val;

    //find the index
    hash<K> keyHash;
    size_t index = keyHash(key);
    index = index % num_buckets;

    (hashTable.at(index).bucketLock).lock();
    for (size_t i = 0; i < (hashTable.at(index).elements).size(); i++) {
      if (key == (hashTable.at(index).elements).at(i).first){
        (hashTable.at(index).bucketLock).unlock();
        return false;
      }
    }
    (hashTable.at(index).elements).push_back(thing);
    (hashTable.at(index).bucketLock).unlock();

    return true;
  }

  /// Insert the provided key/value pair if there is no mapping for the key yet.
  /// If there is a key, then update the mapping by replacing the old value with
  /// the provided value
  ///
  /// @param key The key to upsert
  /// @param val The value to upsert
  ///
  /// @returns true if the key/value was inserted, false if the key already
  ///          existed in the table and was thus updated instead
  bool upsert(K key, V val) { 
    if (insert(key, val))
      return true;
    
    pair<K,V> thing;
    thing.first = key;
    thing.second = val;

    //find the index
    hash<K> keyHash;
    size_t index = keyHash(key);
    index = index % num_buckets;

    (hashTable.at(index).bucketLock).lock();
    for (size_t i = 0; i < (hashTable.at(index).elements).size(); i++) {
      if (key == (hashTable.at(index).elements).at(i).first){
        hashTable.at(index).elements.at(i).second = val;
        (hashTable.at(index).bucketLock).unlock();
        return false;
      }
    }
    (hashTable.at(index).bucketLock).unlock();
    return false; 
  }

  /// Apply a function to the value associated with a given key.  The function
  /// is allowed to modify the value.
  ///
  /// @param key The key whose value will be modified
  /// @param f   The function to apply to the key's value
  ///
  /// @returns true if the key existed and the function was applied, false
  ///          otherwise
  bool do_with(K key, std::function<void(V &)> f) {   

    //find the index
    hash<K> keyHash;
    size_t index = keyHash(key);
    index = index % num_buckets;

    (hashTable.at(index).bucketLock).lock();

    //iterate over elements to find the matching key
    for (size_t i = 0; i < (hashTable.at(index).elements).size(); i++) {
      if (key == (hashTable.at(index).elements).at(i).first){

        //apply the function passed into do_with to the val, with val's address to update val
        f((hashTable.at(index).elements.at(i).second));
        (hashTable.at(index).bucketLock).unlock();
        return true;
      }
    }
    (hashTable.at(index).bucketLock).unlock();
    return false; 
   }

  /// Apply a function to the value associated with a given key.  The function
  /// is not allowed to modify the value.
  ///
  /// @param key The key whose value will be modified
  /// @param f   The function to apply to the key's value
  ///
  /// @returns true if the key existed and the function was applied, false
  ///          otherwise
  bool do_with_readonly(K key, std::function<void(const V &)> f) {

    //find the index
    hash<K> keyHash;
    size_t index = keyHash(key);
    index = index % num_buckets;

    (hashTable.at(index).bucketLock).lock();

    //iterate over elements to find the matching key
    for (size_t i = 0; i < (hashTable.at(index).elements).size(); i++) {
      if (key == (hashTable.at(index).elements).at(i).first){

        /*   apply the function passed into do_with to the val, with val's address but 
        f's parameter is a const V, meaning that val won't be changed   */

        f(hashTable.at(index).elements.at(i).second);
        (hashTable.at(index).bucketLock).unlock();
        return true;
      }
    }
    (hashTable.at(index).bucketLock).unlock();

    return false;
  }

  /// Remove the mapping from a key to its value
  ///
  /// @param key The key whose mapping should be removed
  ///
  /// @returns true if the key was found and the value unmapped, false otherwise
  bool remove(K key) { 
    hash<K> keyHash;
    size_t index = keyHash(key);
    index = index % num_buckets;

    (hashTable.at(index).bucketLock).lock();
    pair<K,V> lastPair;

    //iterate over elements to find the matching key
    for (size_t i = 0; i < (hashTable.at(index).elements).size(); i++) {
        if (key == (hashTable.at(index).elements).at(i).first){

            //get the last pair
            lastPair = (hashTable.at(index).elements).back();

            //replace the pair that's being removed with last pair
            hashTable.at(index).elements.at(i).first = lastPair.first;
            hashTable.at(index).elements.at(i).second = lastPair.second;

            //get rid of last pair that now exists at spot where we removed
            hashTable.at(index).elements.pop_back();
            (hashTable.at(index).bucketLock).unlock();
            return true;
        }
    }
    (hashTable.at(index).bucketLock).unlock();
    return false;

  }

  /// Apply a function to every key/value pair in the ConcurrentHashTable.  Note
  /// that the function is not allowed to modify keys or values.
  ///
  /// @param f    The function to apply to each key/value pair
  /// @param then A function to run when this is done, but before unlocking...
  ///             useful for 2pl
  void do_all_readonly(std::function<void(const K, const V &)> f, std::function<void()> then) {
      //growing phase, aquire locks
      for (int i = 0; i < num_buckets; i ++){
        (hashTable.at(i).bucketLock).lock();
      }

      //iterating through entire hash table
      for (int index = 0; index < num_buckets; index ++){

          //iterating through the elements in each bucket
          for (size_t i = 0; i < (hashTable.at(index).elements).size(); i++) {
              //apply the function passed f onto each KV pair
              f(hashTable.at(index).elements.at(i).first, hashTable.at(index).elements.at(i).second);
          }
      }

      //call the passed then() function before unlocking
      then();

      //shrinking phase, release locks
      for (int i = 0; i < num_buckets; i ++){
        (hashTable.at(i).bucketLock).unlock();
      }
    }
};
