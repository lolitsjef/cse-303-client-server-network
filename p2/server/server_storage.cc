#include <iostream>
#include <openssl/md5.h>
#include <unordered_map>
#include <utility>

#include "../common/contextmanager.h"
#include "../common/err.h"
#include "../common/hashtable.h"
#include "../common/protocol.h"
#include "../common/vec.h"
#include "../common/file.h"

#include "server_storage.h"

using namespace std;

/// Storage::Internal is the private struct that holds all of the fields of the
/// Storage object.  Organizing the fields as an Internal is part of the PIMPL
/// pattern.
struct Storage::Internal {
  /// AuthTableEntry represents one user stored in the authentication table
  struct AuthTableEntry {
    /// The name of the user; max 64 characters
    string username;

    /// The hashed password.  Note that the password is a max of 128 chars
    string pass_hash;

    /// The user's content
    vec content;
  };

  /// A unique 8-byte code to use as a prefix each time an AuthTable Entry is
  /// written to disk.
  inline static const string AUTHENTRY = "AUTHAUTH";

  /// A unique 8-byte code to use as a prefix each time a KV pair is written to
  /// disk.
  inline static const string KVENTRY = "KVKVKVKV";

  /// The map of authentication information, indexed by username
  ConcurrentHashTable<string, AuthTableEntry> auth_table;

  /// The map of key/value pairs
  ConcurrentHashTable<string, vec> kv_store;

  /// filename is the name of the file from which the Storage object was loaded,
  /// and to which we persist the Storage object every time it changes
  string filename = "";

  /// Construct the Storage::Internal object by setting the filename and bucket
  /// count
  ///
  /// @param fname       The name of the file that should be used to load/store
  ///                    the data
  /// @param num_buckets The number of buckets for the hash
  Internal(string fname, size_t num_buckets)
      : auth_table(num_buckets), kv_store(num_buckets), filename(fname) {}
};

/// Construct an empty object and specify the file from which it should be
/// loaded.  To avoid exceptions and errors in the constructor, the act of
/// loading data is separate from construction.
///
/// @param fname       The name of the file that should be used to load/store
///                    the data
/// @param num_buckets The number of buckets for the hash
Storage::Storage(const string &fname, size_t num_buckets)
    : fields(new Internal(fname, num_buckets)) {}

/// Destructor for the storage object.
///
/// NB: The compiler doesn't know that it can create the default destructor in
///     the .h file, because PIMPL prevents it from knowing the size of
///     Storage::Internal.  Now that we have reified Storage::Internal, the
///     compiler can make a destructor for us.
Storage::~Storage() = default;

/// Populate the Storage object by loading this.filename.  Note that load()
/// begins by clearing the maps, so that when the call is complete,
/// exactly and only the contents of the file are in the Storage object.
///
/// @returns false if any error is encountered in the file, and true
///          otherwise.  Note that a non-existent file is not an error.
bool Storage::load() {

  FILE *f = fopen(fields->filename.c_str(), "r");
  if (f == nullptr) {
    cerr << "File not found: " << fields->filename << endl;
    return true;
  }

  ContextManager fileCloser([&] () { fclose(f); });
  vec fileStuff;
  fileStuff = load_entire_file((fields -> filename));
  cerr << "Loaded: " << (fields -> filename) << endl;

  //Vecs of stuff 
  size_t index = 0;   //current place in the file vector

  while(true){
    string entryType;  //KVKVKVKV or AUTHAUTH
    size_t keySize; //SIZE of Key
    string key;        //string of the key
    size_t valSize; //SIZE of val
    vec val;           //vector of the val

    size_t userSize; //SIZE of username
    string user;        //string of the username
    size_t passSize; //SIZE of password
    string pass;        //string of the password
    size_t conSize;  //SIZE of content
    vec con;         //vector of the content

    //populate entry type to KVKVKVKV or AUTHAUTH

    for (size_t i = 0; i < 8; i++){
      entryType+= (char)(fileStuff.at(index++));
    }
    //if a KV entry
    if (entryType == "KVKVKVKV"){
      //get the key
      int a = fileStuff.at(index++);
      int b = fileStuff.at(index++) << 8;
      int c = fileStuff.at(index++) << 16;
      int d = fileStuff.at(index++) << 24;
      keySize = a|b|c|d;
      

      for (size_t i = 0; i < keySize; i++){
        key+= (char)(fileStuff.at(index++));
      }


      //get the val
      a = fileStuff.at(index++);
      b = fileStuff.at(index++) << 8;
      c = fileStuff.at(index++) << 16;
      d = fileStuff.at(index++) << 24;
      valSize = a|b|c|d;


      for (size_t i = 0; i < valSize; i++){
        val.push_back(fileStuff.at(index++));
      }

      //insert the key and the value into kv_store
      
      fields -> kv_store.insert(key, val);
    }

    //if an auth entry
    else if (entryType == "AUTHAUTH"){ 
      //get the username
      int a = fileStuff.at(index++);
      int b = fileStuff.at(index++) << 8;
      int c = fileStuff.at(index++) << 16;
      int d = fileStuff.at(index++) << 24;
      userSize = a|b|c|d;
      
      for (size_t i = 0; i < userSize; i++){
        user+= (char)fileStuff.at(index++);
      }

      //get the password
      a = fileStuff.at(index++);
      b = fileStuff.at(index++) << 8;
      c = fileStuff.at(index++) << 16;
      d = fileStuff.at(index++) << 24;
      passSize = a|b|c|d;
      for (size_t i = 0; i < passSize; i++){
        pass+= (char)fileStuff.at(index++);
      }

      //get the content
      a = fileStuff.at(index++);
      b = fileStuff.at(index++) << 8;
      c = fileStuff.at(index++) << 16;
      d = fileStuff.at(index++) << 24;
      conSize = a|b|c|d;

      for (size_t i = 0; i < conSize; i++){
        con.push_back(fileStuff.at(index++));
      }

      Internal::AuthTableEntry entry;
      entry.username = user;
      entry.pass_hash = pass;
      entry.content = con;

      //insert the key and the value into kv_store
      fields -> auth_table.insert(user, entry);

    }
    else {
      cerr << "Error parsing: " << fields->filename << endl;
      return false;
    }
    if (index >= fileStuff.size())
      break;
  }
  return true;
}

/// Create a new entry in the Auth table.  If the user_name already exists, we
/// should return an error.  Otherwise, hash the password, and then save an
/// entry with the username, hashed password, and a zero-byte content.
///
/// @param user_name The user name to register
/// @param pass      The password to associate with that user name
///
/// @returns False if the username already exists, true otherwise
bool Storage::add_user(const string &user_name, const string &pass) {
  
  //create a new auth table entry
  Internal::AuthTableEntry newUser;
  newUser.username = user_name;
  unsigned char* hashedPass = NULL;
  hashedPass = MD5((unsigned char*) pass.data(), pass.length(), NULL);
  string hashedPassString(reinterpret_cast<char*>(hashedPass));
  newUser.pass_hash = hashedPassString;

  //insert the auth table entry
  if (fields -> auth_table.insert(user_name, newUser))
    return true;
  return false;
}

/// Set the data bytes for a user, but do so if and only if the password
/// matches
///
/// @param user_name The name of the user whose content is being set
/// @param pass      The password for the user, used to authenticate
/// @param content   The data to set for this user
///
/// @returns A pair with a bool to indicate error, and a vector indicating the
///          message (possibly an error message) that is the result of the
///          attempt
vec Storage::set_user_data(const string &user_name, const string &pass, const vec &content) {
  unsigned char* hashedPass = NULL;
  hashedPass = MD5((unsigned char*) pass.data(), pass.length(), NULL);
  string hashedPassString(reinterpret_cast<char*>(hashedPass));
  
  //lambda that will update auth if the user authenticates and update the data of said user if they authenticate
  bool auth = false;
  auto updateData = [&] (Internal::AuthTableEntry userData) {
    if (userData.pass_hash == hashedPassString){
      auth = true;
      userData.content = content;    
    }
  };  

  //check if user exists and if they authenticated
  if (!((fields -> auth_table).do_with(user_name, updateData)))
    return vec_from_string(RES_ERR_NO_USER);
  if (!auth)
    return vec_from_string(RES_ERR_LOGIN);
  //return ok if they authenticated
  return vec_from_string(RES_OK);
}

/// Return a copy of the user data for a user, but do so only if the password
/// matches
///
/// @param user_name The name of the user who made the request
/// @param pass      The password for the user, used to authenticate
/// @param who       The name of the user whose content is being fetched
///
/// @returns A pair with a bool to indicate error, and a vector indicating the
///          data (possibly an error message) that is the result of the
///          attempt.  Note that "no data" is an error
pair<bool, vec> Storage::get_user_data(const string &user_name, const string &pass, const string &who) {

  //lambda to authenticate user
  if (!auth(user_name, pass))
    return {true, vec_from_string(RES_ERR_LOGIN)};      

  //lambda to get the content vec of who
  vec whoContent;
  auto getData = [&] (Internal::AuthTableEntry userData) {
      whoContent = userData.content;    
    };   

  //if who doesnt exist return error                          
  if (!((fields -> auth_table).do_with_readonly(who, getData)))
    return {true, vec_from_string(RES_ERR_NO_USER)};      
       
  return {false, whoContent};
}

/// Return a newline-delimited string containing all of the usernames in the
/// auth table
///
/// @param user_name The name of the user who made the request
/// @param pass      The password for the user, used to authenticate
///
/// @returns A vector with the data, or a vector with an error message
pair<bool, vec> Storage::get_all_users(const string &user_name, const string &pass) {
  //authenticate user
  if (!auth(user_name, pass))
    return {true, vec_from_string(RES_ERR_LOGIN)};      

  
  vec allStuff;
  auto getAllStuff = [&] (string user, Internal::AuthTableEntry entry) {
    vec_append(allStuff, entry.username);
    allStuff.push_back('\n');
  };  

  auto then = [] () {
  }; 

  //if user doesnt exist or doesnt authenticate then return an error
  (fields -> auth_table).do_all_readonly(getAllStuff, then);
  if (allStuff.size() == 0)
    return {true, vec_from_string(RES_ERR_NO_DATA)};
  return {false, allStuff};
  
  //return {true, vec_from_string(RES_ERR_NO_DATA)};
}

/// Authenticate a user
///
/// @param user_name The name of the user who made the request
/// @param pass      The password for the user, used to authenticate
///
/// @returns True if the user and password are valid, false otherwise
bool Storage::auth(const string &user_name, const string &pass) {
  
  unsigned char* hashedPass = NULL;
  hashedPass = MD5((unsigned char*) pass.data(), pass.length(), NULL);
  string hashedPassString(reinterpret_cast<char*>(hashedPass));

  bool auth = false;
  auto authorizeUser = [&] (const Internal::AuthTableEntry userData) {
    if (userData.pass_hash == hashedPassString){
      auth = true;
    }
  };  
  //if user doesnt exist or doesnt authenticate then return an error
  (fields -> auth_table).do_with(user_name, authorizeUser);
  return auth;
}

/// Write the entire Storage object to the file specified by this.filename.
/// To ensure durability, Storage must be persisted in two steps.  First, it
/// must be written to a temporary file (this.filename.tmp).  Then the
/// temporary file can be renamed to replace the older version of the Storage
/// object.
void Storage::persist() {
  string tempfile = "tempfile.tmp";
    vec KVstuff;
    vec authStuff;

  auto printKVStuff = [&] (const string key, const vec value) {
    vec_append(KVstuff, (fields -> KVENTRY));
    vec_append(KVstuff, key.size());
    vec_append(KVstuff, key);
    vec_append(KVstuff, value.size());
    vec_append(KVstuff, value); 
     };
  //print auth table
  auto printAuthStuff = [&] (const string key, Internal::AuthTableEntry userEntry){
    //make vec of stuff to print for each auth table entry
    vec_append(authStuff, (fields -> AUTHENTRY));
    vec_append(authStuff, userEntry.username.size());
    vec_append(authStuff, userEntry.username);
    vec_append(authStuff, userEntry.pass_hash.size());
    vec_append(authStuff, userEntry.pass_hash);
    vec_append(authStuff, userEntry.content.size());
    vec_append(authStuff, userEntry.content);

    //write to the file
    write_file(tempfile, (const char*) authStuff.data(), authStuff.size());
  };
  //rename the file
  auto nothing = [&] () {};
  //calls the write function and then calls the renamefile function
  auto doKVwrite = [&] () {
  (fields -> kv_store).do_all_readonly(printKVStuff, nothing);
  }; 

  //start lambda chain by printing auth table and then calling kvwrite
  (fields -> auth_table).do_all_readonly(printAuthStuff, doKVwrite);
  //append the KV and authtable
  vec_append(authStuff, KVstuff);
  //write them
  write_file(tempfile, (const char*) authStuff.data(), authStuff.size());
  rename(tempfile.c_str(), (fields -> filename).c_str());


}

/// Shut down the storage when the server stops.
///
/// NB: this is only called when all threads have stopped accessing the
///     Storage object.  As a result, there's nothing left to do, so it's a
///     no-op.
void Storage::shutdown() {
}

/// Create a new key/value mapping in the table
///
/// @param user_name The name of the user who made the request
/// @param pass      The password for the user, used to authenticate
/// @param key       The key whose mapping is being created
/// @param val       The value to copy into the map
///
/// @returns A vec with the result message
vec Storage::kv_insert(const string &user_name, const string &pass, const string &key, const vec &val) {
  if (!auth(user_name, pass))
    return vec_from_string(RES_ERR_LOGIN); 

  if (!fields -> kv_store.insert(key, val))
    return vec_from_string(RES_ERR_KEY);
  return vec_from_string(RES_OK);
};

/// Get a copy of the value to which a key is mapped
///
/// @param user_name The name of the user who made the request
/// @param pass      The password for the user, used to authenticate
/// @param key       The key whose value is being fetched
///
/// @returns A pair with a bool to indicate error, and a vector indicating the
///          data (possibly an error message) that is the result of the
///          attempt.
pair<bool, vec> Storage::kv_get(const string &user_name, const string &pass, const string &key) {

  //authenticate user
  if (!auth(user_name, pass))
    return {true, vec_from_string(RES_ERR_LOGIN)};      

  //lambda to get the content vec of who
  vec content;
  auto getData = [&] (vec value) {
      content = value;    
    };   

  //if who doesnt exist return error                          
  if (!((fields -> kv_store).do_with_readonly(key, getData)))
    return {true, vec_from_string(RES_ERR_KEY)};      
       
  return {false, content};
};

/// Delete a key/value mapping
///
/// @param user_name The name of the user who made the request
/// @param pass      The password for the user, used to authenticate
/// @param key       The key whose value is being deleted
///
/// @returns A vec with the result message
vec Storage::kv_delete(const string &user_name, const string &pass, const string &key) {
  //authenticate user
  if (!auth(user_name, pass))
    return vec_from_string(RES_ERR_LOGIN); 

  //if the key exists remove the key
  if (!fields -> kv_store.remove(key))
    return vec_from_string(RES_ERR_KEY);
  return vec_from_string(RES_OK);
};

/// Insert or update, so that the given key is mapped to the give value
///
/// @param user_name The name of the user who made the request
/// @param pass      The password for the user, used to authenticate
/// @param key       The key whose mapping is being upserted
/// @param val       The value to copy into the map
///
/// @returns A vec with the result message.  Note that there are two "OK"
///          messages, depending on whether we get an insert or an update.
vec Storage::kv_upsert(const string &user_name, const string &pass, const string &key, const vec &val) {
  if (!auth(user_name, pass))
    return vec_from_string(RES_ERR_LOGIN); 

  if (!fields -> kv_store.upsert(key, val))
    return vec_from_string("OKUPD");
  return vec_from_string("OKINS");
};

/// Return all of the keys in the kv_store, as a "\n"-delimited string
///
/// @param user_name The name of the user who made the request
/// @param pass      The password for the user, used to authenticate
///
/// @returns A pair with a bool to indicate errors, and a vec with the result
///          (possibly an error message).
pair<bool, vec> Storage::kv_all(const string &user_name, const string &pass) {
  //authenticate user
  if (!auth(user_name, pass))
    return {true, vec_from_string(RES_ERR_LOGIN)};      

  
  vec allStuff;
  auto getAllStuff = [&] (string key, vec value) {
    vec_append(allStuff, key);
    allStuff.push_back('\n');
  };  

  auto then = [] () {
  }; 

  //if user doesnt exist or doesnt authenticate then return an error
  (fields -> kv_store).do_all_readonly(getAllStuff, then);
  if (allStuff.size() == 0)
    return {true, vec_from_string(RES_ERR_NO_DATA)};
  return {false, allStuff};
};
